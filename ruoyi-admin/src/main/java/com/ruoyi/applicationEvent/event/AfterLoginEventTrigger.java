package com.ruoyi.applicationEvent.event;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.ruoyi.applicationEvent.ApplicationEvent;
import com.ruoyi.applicationEvent.ApplicationEventDefined;
import com.ruoyi.applicationEvent.IApplicationEvent;

import lombok.extern.slf4j.Slf4j;

/**
 * 用户登录后触发
 */
@Slf4j
@Component
@ApplicationEvent({ ApplicationEventDefined.ON_AFTER_LOGIN })
public class AfterLoginEventTrigger implements IApplicationEvent {
    @Override
    public void onTrigger(Object source, Object params) {
        log.info("用户登陆后：系统任务被触发：" + source.toString() + "\t\t" + JSON.toJSONString(params));
    }
}