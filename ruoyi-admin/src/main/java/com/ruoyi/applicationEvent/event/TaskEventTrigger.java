package com.ruoyi.applicationEvent.event;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.ruoyi.applicationEvent.ApplicationEvent;
import com.ruoyi.applicationEvent.ApplicationEventDefined;
import com.ruoyi.applicationEvent.IApplicationEvent;

import lombok.extern.slf4j.Slf4j;

/**
 * 定时任务被触发或者被手工触发
 */
@Slf4j
@Component
@ApplicationEvent({ ApplicationEventDefined.ON_SCHEDULER_EXECUTED,
        ApplicationEventDefined.ON_SCHEDULER_EXECUTED_BY_HAND })
public class TaskEventTrigger implements IApplicationEvent {
    @Override
    public void onTrigger(Object source, Object params) {
        log.info("系统任务被触发：" + source.toString() + "\t\t" + JSON.toJSONString(params));
    }
}
