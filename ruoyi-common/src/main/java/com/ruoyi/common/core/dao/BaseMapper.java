/*
 * @(#)BaseMapper.java 2016-3-30 下午5:57:15
 * Copyright 2016 张孟如, Inc. All rights reserved. 
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.ruoyi.common.core.dao;

import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

/**
 * 基础的Mapper接口
 * 
 * @author 张孟如
 * @version 1.0
 * @since Copyright (c) 2016 2016-3-30 下午5:57:15
 */
public interface BaseMapper<T> extends Mapper<T>, IdsMapper<T>, InsertListMapper<T>, ConditionMapper<T> {
}