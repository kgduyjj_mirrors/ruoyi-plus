package com.ruoyi.ehcache.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.ruoyi.ehcache.util.EhCacheUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * springboot 允许实现CommandLineRunner接口的类程序启动后run方法中做一些事情，比如加载缓存.
 */
@Slf4j
@Component
@Order(value = 1)
public class InitEhCacheRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        // TODO 可以从数据库加载配置信息到缓存
        log.info("======= 开始：加载Ehcache缓存信息 =======");
        EhCacheUtils.putSysInfo("test_key", "test_value");
        EhCacheUtils.putUserInfo("test_user_key", "1", "test_user_value");
        EhCacheUtils.putDefaultInfo("test_default_key", "test_default_value");
        log.info("======= 结束：加载Ehcache缓存信息 =======");
    }
}
