package com.ruoyi.oss.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 文件上传
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class SysOss implements Serializable {
    //
    private static final long serialVersionUID = 1356257283938225230L;

    @Id
    private Long id;

    /** 文件名 */
    private String fileName;

    /** 文件后缀 */
    private String fileSuffix;

    /** URL地址 */
    private String url;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /** 上传者 */
    private String createBy;

    /** 服务商 */
    private Integer service;

}
