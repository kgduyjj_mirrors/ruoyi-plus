package com.ruoyi.redis.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.redis.util.InitRedisUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 读取代码生成相关配置
 * 
 * @author wujiyue
 */
@Slf4j
@Configuration
@PropertySource(value = { "classpath:redis.properties" })
public class RedisConfig {
    @Value("${redis.address}")
    protected String redisAddress;

    @PostConstruct
    public void initRedis() {
        String[] redisAddresses = StringUtils.split(redisAddress, ",");
        for (String redisAddress : redisAddresses) {
            log.debug("=======  Redis Address : [ {} ] =======", redisAddress);
        }
        InitRedisUtil.init(redisAddress);
        log.info("=======  Redis Init : [ {} ]  =======", (InitRedisUtil.initFlag ? "Success" : "Failure"));
    }
}
